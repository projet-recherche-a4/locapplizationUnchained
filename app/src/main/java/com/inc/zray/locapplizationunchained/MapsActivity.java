package com.inc.zray.locapplizationunchained;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Tile;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.google.android.gms.maps.model.UrlTileProvider;
import com.lemmingapex.trilateration.NonLinearLeastSquaresSolver;
import com.lemmingapex.trilateration.TrilaterationFunction;

import org.apache.commons.math3.fitting.leastsquares.LeastSquaresOptimizer;
import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private CheckBox toggleScanButton, toggleMarkers;
    private static final int ACCESS_FINE_LOCATION_REQUEST = 1;
    private static final int ACCESS_COARSE_LOCATION_REQUEST = 1;
    private WifiManager wifiManager;
    private static final String TAG = "Application";
    private TextView text;
    private TileOverlay mMapTiles;
    /**
     * Format : tiles/{rdc,floor1}/[zoom]_[x]_[y].png
     */
    private static final String MAP_URL_FORMAT = "https://mimir.infra.remyj.fr/tiles/%s/%d_%d_%d.png";
    private static final String MAP_FLOOR_RDC = "rdc";
    private static final String MAP_FLOOR_FLOOR1 = "floor1";
    private String currentFloor = MAP_FLOOR_FLOOR1;
    private WifiLocationSource mLocationSource;
    private static String accessPoints = "{'c0c1c0e80656': {lat: -0.18360, long: -78.55568}, " +
            "'c0c1c0e80413': {lat: -0.18262, long: -78.55628}," +
            "'b827eb647c40': {lat: -0.18305, long: -78.55785}," +
            "'b827eb2c9f9e': {lat: -0.18506, long: -78.55672}," +
            "'b827eba14eef': {lat: -0.18110, long: -78.55672}}";
    private Marker[] markers;
    private JSONObject accessPointsJSON;
    private static final double ratioGpsReal = 0.000425;


    /**
     * A {@link LocationSource} which reports a new location whenever a user long presses the map
     * at
     * the point at which a user long pressed the map.
     */
    private static class WifiLocationSource implements LocationSource, GoogleMap.OnMapLongClickListener {

        private OnLocationChangedListener mListener;

        /**
         * Flag to keep track of the activity's lifecycle. This is not strictly necessary in this
         * case because onMapLongPress events don't occur while the activity containing the map is
         * paused but is included to demonstrate best practices (e.g., if a background service were
         * to be used).
         */
        private boolean mPaused;

        @Override
        public void activate(OnLocationChangedListener listener) {
            mListener = listener;
        }

        @Override
        public void deactivate() {
            mListener = null;
        }

        @Override
        public void onMapLongClick(LatLng point) {
            if (mListener != null && !mPaused) {
                Location location = new Location("WifiLocationProvider");
                location.setLatitude(point.latitude);
                location.setLongitude(point.longitude);
                location.setAccuracy(100);
                mListener.onLocationChanged(location);
                Log.d(TAG, "Lat/Long: " + point.latitude + ", " + point.longitude);
            }
        }

        public void setPosition(double[] position){
            Location location = new Location("WifiLocationProvider");
            location.setLatitude(position[1]);
            location.setLongitude(position[0]);
            location.setAccuracy(10);
            mListener.onLocationChanged(location);
        }

        public void onPause() {
            mPaused = true;
        }

        public void onResume() {
            mPaused = false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        try {
            accessPointsJSON = new JSONObject(accessPoints);
        } catch (JSONException e) {
            Log.d(TAG, "Unable to load JSON");
        }
        toggleScanButton = (CheckBox) findViewById(R.id.toggleScanButton);
        toggleMarkers = (CheckBox) findViewById(R.id.toggleMarkersButton);
        text = (TextView) findViewById(R.id.text);
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        mLocationSource = new WifiLocationSource();
        toggleScanButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggleScanButton.setText((toggleScanButton.isChecked()) ? "Stop scan" : "Start scan");
                if (toggleScanButton.isChecked()) {
                    if (!wifiManager.isWifiEnabled()) {
                        text.setText("Please enable WiFi");
                    } else {
                        // Register event when scan results will be available
                        registerReceiver(mWifiScanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
                        Log.d(TAG, "Starting scan");
                        wifiManager.startScan();
                    }
                }
            }
        });
        toggleMarkers.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                for(Marker current : markers){
                    current.setVisible(toggleMarkers.isChecked());
                }
                toggleMarkers.setText((toggleMarkers.isChecked()) ? "Hide Markers" : "Display Markers");
            }
        });
        askPermissions();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-0.18262, -78.55631), 15));
        mMap.setMinZoomPreference(13.0f);
        mMap.setMaxZoomPreference(15.0f);
        mMap.setPadding(0, 300, 0, 0);

        addMarkers();
        /* Custom source location */
        mMap.setLocationSource(mLocationSource);
        mMap.setOnMapLongClickListener(mLocationSource);
        if (!(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            mMap.setMyLocationEnabled(true);
        }
        /* End custom source location */

        /* Custom tiles */
        TileProvider tileProvider = new UrlTileProvider(256, 256) {
            @Override
            public synchronized URL getTileUrl(int x, int y, int zoom) {
                String s = String.format(Locale.US, MAP_URL_FORMAT,currentFloor, zoom, x, y);
                URL url = null;
                try {
                    url = new URL(s);
                } catch (MalformedURLException e) {
                    throw new AssertionError(e);
                }
                return url;
            }
        };

        mMapTiles = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(tileProvider));
        /* End custom tiles */
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLocationSource.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLocationSource.onPause();
    }

    /* -------------------------------------------------------------------------------------------------------------------------------------- */

    /**
     * Determining distance from decibel level
     * There’s a useful concept in physics that lets us mathematically relate the signal level in dB to a real-world distance.  Free-space path loss (FSPL) characterizes how the wireless signal degrades over distance (following an inverse square law):
     * FSPL(Db) = 20*log10(d)+ 20*log10(f) + 27.55
     */
    public static Double calculateDistanceInMeters(int levelInDb, int freqInMHz)    {
        double exp = (27.55 - (20 * Math.log10(freqInMHz)) + Math.abs(levelInDb)) / 20.0;
        return Math.pow(10.0, exp);
    }

    private final BroadcastReceiver mWifiScanReceiver = new BroadcastReceiver() {
        /**
         * Event called when Wifi scan is complete
         */
        @Override
        public void onReceive(Context c, Intent intent) {
            if (toggleScanButton.isChecked() && intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                Comparator<ScanResult> comparator = new Comparator<ScanResult>() {
                    @Override
                    public int compare(ScanResult lhs, ScanResult rhs) {
                        return (lhs.level >rhs.level ? -1 : (lhs.level==rhs.level ? 0 : 1));
                    }
                };
                List<ScanResult> information = wifiManager.getScanResults();
                Collections.sort(information, comparator);
                String textTemp = "";
                List<Double[]> knownAccessPoints = new ArrayList<>();
                List<Double> distances = new ArrayList<>();
                for(ScanResult info : information){
                    JSONObject current = null;
                    try {
                        current = accessPointsJSON.getJSONObject(info.BSSID.toLowerCase().replace(":", ""));
                        Double[] position = new Double[2];
                        position[0] = current.getDouble("long");
                        position[1] = current.getDouble("lat");
                        knownAccessPoints.add(position);
                        distances.add(realLengthToGpsLength(calculateDistanceInMeters(info.level, info.frequency)));
                    } catch (JSONException e) {}
                    if (current != null) {
                        textTemp = textTemp + ", BSSID: " + info.BSSID + ", RSSI: " + info.level + "\n";
                        Log.d(TAG, "SSID: " + info.SSID + ", BSSID: " + info.BSSID + ", RSSI: " + info.level + ", Distance: " + calculateDistanceInMeters(info.level, info.frequency));
                    } else {
                        //Log.d(TAG, "Unknown BSSID " + info.BSSID);
                    }
                }

                Double[][] knownAccessPointsTable = new Double[knownAccessPoints.size()][2];
                for(int i = 0; i < knownAccessPoints.size(); i++)
                {
                    knownAccessPointsTable[i] = knownAccessPoints.get(i);
                }
                Double[] distancesTable = new Double[distances.size()];
                for(int i = 0; i < distances.size(); i++)
                {
                    distancesTable[i] = distances.get(i);
                }

                text.setText(textTemp);
                try {
                    NonLinearLeastSquaresSolver solver = new NonLinearLeastSquaresSolver(new TrilaterationFunction(knownAccessPointsTable, distancesTable), new LevenbergMarquardtOptimizer());
                    LeastSquaresOptimizer.Optimum optimum = solver.solve();
                    double[] centroid = optimum.getPoint().toArray();
                    Log.d(TAG, "Result (Lat/Long): " + centroid[1] + ", " + centroid[0]);
                    mLocationSource.setPosition(centroid);
                } catch (Exception e){

                }
                wifiManager.startScan();
            } else if (!toggleScanButton.isChecked()){
                Log.d(TAG, "Scan stopped");
            }
        }
    };

    /**
     * Permissions event triggered when user reply to request(for Android 6)
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
                    if (!service.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                        showGPSDisabledAlertToUser();
                    }
                } else {
                    text.setText("Please enable GPS !");
                }
                return;
            }
        }
    }

    /**
     * Ask user to grant permissions if not already granted
     */
    private void askPermissions(){

        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            if (!ActivityCompat.shouldShowRequestPermissionRationale((Activity) this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions((Activity) this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        ACCESS_FINE_LOCATION_REQUEST);
            }
            if (!ActivityCompat.shouldShowRequestPermissionRationale((Activity) this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                ActivityCompat.requestPermissions((Activity) this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        ACCESS_COARSE_LOCATION_REQUEST);
            }
        }
    }

    /**
     * Show message to user to enable Localization
     * Source : https://stackoverflow.com/a/7990939
     */
    private void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Localization is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable Localization",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private Double gpsLengthToRealLength(Double realLength)
    {
        return Math.floor(realLength / ratioGpsReal);
    }

    private Double realLengthToGpsLength(Double realLength)
    {
        return realLength * ratioGpsReal;
    }

    private Double lengthBetween(Double firstPointX, Double firstPointY, Double secondPointX, Double secondPointY)
    {
        return Math.sqrt(Math.pow(gpsLengthToRealLength(secondPointX) - gpsLengthToRealLength(firstPointX), 2) + Math.pow(gpsLengthToRealLength(secondPointY) - gpsLengthToRealLength(firstPointY), 2));
    }

    private void addMarkers(){
        Iterator<?> keys = accessPointsJSON.keys();
        markers = new Marker[accessPointsJSON.length()];
        int i = 0;
        while( keys.hasNext() ) {
            String key = (String)keys.next();
            try {
                if ( accessPointsJSON.get(key) instanceof JSONObject ) {
                    JSONObject current = accessPointsJSON.getJSONObject(key);
                    LatLng position = new LatLng(current.getDouble("lat"), current.getDouble("long"));
                    markers[i] = mMap.addMarker(new MarkerOptions().position(position).title(key).visible(toggleMarkers.isChecked()));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            i++;
        }
    }

    /* -------------------------------------------------------------------------------------------------------------------------------------- */

    private static class CoordTileProvider implements TileProvider {

        private static final int TILE_SIZE_DP = 256;

        private final float mScaleFactor;

        private final Bitmap mBorderTile;

        public CoordTileProvider(Context context) {
            /* Scale factor based on density, with a 0.6 multiplier to increase tile generation
             * speed */
            mScaleFactor = context.getResources().getDisplayMetrics().density * 0.6f;
            Paint borderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            borderPaint.setStyle(Paint.Style.STROKE);
            mBorderTile = Bitmap.createBitmap((int) (TILE_SIZE_DP * mScaleFactor),
                    (int) (TILE_SIZE_DP * mScaleFactor), android.graphics.Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(mBorderTile);
            canvas.drawRect(0, 0, TILE_SIZE_DP * mScaleFactor, TILE_SIZE_DP * mScaleFactor,
                    borderPaint);
        }

        @Override
        public Tile getTile(int x, int y, int zoom) {
            Bitmap coordTile = drawTileCoords(x, y, zoom);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            coordTile.compress(Bitmap.CompressFormat.PNG, 0, stream);
            byte[] bitmapData = stream.toByteArray();
            return new Tile((int) (TILE_SIZE_DP * mScaleFactor),
                    (int) (TILE_SIZE_DP * mScaleFactor), bitmapData);
        }

        private Bitmap drawTileCoords(int x, int y, int zoom) {
            // Synchronize copying the bitmap to avoid a race condition in some devices.
            Bitmap copy = null;
            synchronized (mBorderTile) {
                copy = mBorderTile.copy(android.graphics.Bitmap.Config.ARGB_8888, true);
            }
            Canvas canvas = new Canvas(copy);
            String tileCoords = "(" + x + ", " + y + ")";
            String zoomLevel = "zoom = " + zoom;
            /* Paint is not thread safe. */
            Paint mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mTextPaint.setTextAlign(Paint.Align.CENTER);
            mTextPaint.setTextSize(18 * mScaleFactor);
            canvas.drawText(tileCoords, TILE_SIZE_DP * mScaleFactor / 2,
                    TILE_SIZE_DP * mScaleFactor / 2, mTextPaint);
            canvas.drawText(zoomLevel, TILE_SIZE_DP * mScaleFactor / 2,
                    TILE_SIZE_DP * mScaleFactor * 2 / 3, mTextPaint);
            return copy;
        }
    }
}

